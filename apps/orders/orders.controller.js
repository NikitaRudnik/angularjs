import { NEW_ORDER } from './orders.constants.js';

function ordersController($scope, dataFormSave) {
    $scope.ordersArray = dataFormSave.loadList();
    $scope.pageState = 'order';
    $scope.changePageState = (state) => {
        $scope.pageState = state;
    }
    $scope.newOrder = {...NEW_ORDER };
    $scope.order = {...NEW_ORDER };
    $scope.show = () => console.log($scope);
    $scope.save = () => {
        dataFormSave.saveOrder($scope.order);
        $scope.ordersArray = dataFormSave.loadList();
        $scope.order = {...NEW_ORDER };
    }
};

export default ordersController;