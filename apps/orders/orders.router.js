const router = (routeProvider) => {
  routeProvider
    .when(
      '/order', {
        templateUrl: '/apps/orders/components/orderForm/orderForm.tmpl.html',
      },
    )
    .when(
      '/summary', {
        templateUrl: '/apps/orders/components/summaryForm/summaryForm.tmpl.html',
      },
    )
    .otherwise({
      // redirectTo: '/order',
    });
};

export default router;
