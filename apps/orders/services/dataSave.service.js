import { NEW_ORDER } from '../orders.constants.js';

const dataService = () => {
  const service = {};
  let temlpeData = { ...NEW_ORDER };
  const orderList = [];
  service.tmpSave = (orderObject) => {
    temlpeData = { ...orderObject };
  };
  service.tmpLoad = () => temlpeData;
  service.saveOrder = (orderObject) => {
    orderList.push({ ...orderObject });
  };
  service.loadList = () => orderList;
  return service;
};

export default dataService;
