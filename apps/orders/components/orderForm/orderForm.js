import orderFormController from './orderForm.controller.js';

const orderForm = {
    templateUrl: '/apps/orders/components/orderForm/orderForm.tmpl.html',
    controller: orderFormController,
    bindings: {
        order: '='
    },
};

export default orderForm;