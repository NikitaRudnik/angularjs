import ordersListController from './ordersList.controller.js';

const ordersList = {
    templateUrl: '/apps/orders/components/ordersList/ordersList.tmpl.html',
    controller: ordersListController,
    bindings: {
        ordersList: '='
    }
};
export default ordersList;