import previewController from './preview.controller.js';

const appPreview = {
    templateUrl: '/apps/orders/components/preview/preview.tmpl.html',
    controller: previewController,
    bindings: {
        inputUser: '<',
        inputAdress: '<',
        order: '='
    },
};
export default appPreview;