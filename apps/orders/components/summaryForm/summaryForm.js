import summaryFormController from './summaryForm.controller.js';

const summaryForm = {
    templateUrl: '/apps/orders/components/summaryForm/summaryForm.tmpl.html',
    controller: summaryFormController,
    bindings: {
        order: '='
    },
};
export default summaryForm;