const headerController = ($scope) => {
  const parentScope = $scope.$parent;
  $scope.saveOrder = parentScope.saveOrder;
  $scope.save = () => {
    $scope.$emit('save');
  };
};

export default headerController;
