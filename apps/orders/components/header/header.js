import headerController from './header.controller.js';

const appHeader = {
  templateUrl: '/apps/orders/components/header/header.tmpl.html',
  controller: headerController,
  bindings: {
    save: '&',
    out: '&',
  },
};
export default appHeader;
