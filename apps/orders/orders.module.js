import dataService from './services/dataSave.service.js';
import numberValidator from './directives/numberValidator.js';
import emailValidator from './directives/emailValidator.js';
import textValidator from './directives/textValidator.js';
import ordersController from './orders.controller.js';
import ordersList from './components/ordersList/ordersLists.js';
import summaryForm from './components/summaryForm/summaryForm.js';
import orderForm from './components/orderForm/orderForm.js';
import preview from './components/preview/preview.js';
import appHeader from './components/header/header.js';

const orderModule = angular.module('orderModule', ['ngRoute']);
orderModule.factory('dataSaveService', dataService);
orderModule.controller('ordersAppController', ['$scope', 'dataSaveService', ($scope, dataSaveService) => { ordersController($scope, dataSaveService) }]);
orderModule.directive('numberValidator', numberValidator);
orderModule.directive('emailValidator', emailValidator);
orderModule.directive('textValidator', textValidator);
orderModule.component('appOrdersModule', { templateUrl: '/apps/orders/orders.tmpl.html' });
orderModule.component('appOrdersList', ordersList);
orderModule.component('appOrderForm', orderForm);
orderModule.component('appSummary', summaryForm);
orderModule.component('appPreview', preview);
orderModule.component('appHeader', appHeader);

export default orderModule;