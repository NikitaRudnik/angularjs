const numberValidator = () => ({
  require: 'ngModel',
  link(scope, elm, attrs, ctrl) {
    const obj = ctrl;
    obj.$validators.numberValidator = (modelValue, viewValue) => {
      const valid = Number.isInteger(+viewValue);
      return valid;
    };
  },
});

export default numberValidator;
