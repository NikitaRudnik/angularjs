const textValidator = () => ({
  require: 'ngModel',
  link(scope, elm, attrs, ctrl) {
    const obj = ctrl;
    obj.$validators.textValidator = (modelValue, viewValue) => {
      let errors = 0;
      const rule = new RegExp('[^a-zA-Z]+');
      if (viewValue) {
        viewValue.split('').forEach((item) => {
          if (rule.test(item)) { errors += 1; }
        });
      }
      return !errors;
    };
  },
});

export default textValidator;
