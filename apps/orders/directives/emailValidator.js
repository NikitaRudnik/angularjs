const emailValidator = () => ({
  require: 'ngModel',
  link(scope, elm, attrs, ctrl) {
    const obj = ctrl;
    obj.$validators.emailValidator = (modelValue, viewValue) => {
      const rule = new RegExp('[a-z0-9!#$%&\'*+/=?^_`{|}~.-]+@[a-z0-9-]+(.[a-z0-9-]+)*');
      console.log(rule.test(viewValue));
      return rule.test(viewValue);
    };
  },
});

export default emailValidator;
